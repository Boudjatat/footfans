from django.db import models

# Create your models here.
class Division(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False, verbose_name="name")
    country = models.CharField(max_length=50, blank=False, null = False, verbose_name="country")
    link = models.URLField()

    class Meta:
        unique_together = ('name','country')
        verbose_name = "division"
        verbose_name_plural = "division"
       
    
    def __str__(self):
        return self.name

class Club(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False, verbose_name="name")
    division = models.ForeignKey('Division',on_delete=models.CASCADE)
    link = models.URLField()

    class Meta:
        unique_together = ('name','division')
        verbose_name = "club"
        verbose_name_plural = "clubs"
    
    def __str__(self):
        return self.name


class Player(models.Model):
    number=models.CharField(max_length=2, default="0")
    name=models.CharField(max_length=100, blank=True, null=True,verbose_name="name")
    country=models.CharField(max_length=3)
    position=models.CharField(max_length=2,blank=True,verbose_name="position")
    club=models.ForeignKey('Club',on_delete=models.CASCADE)

    class Meta:
        unique_together = ('number','position','name','club')
        verbose_name = "player"
        verbose_name_plural = "players"
        ordering = ["club"]

    def __str__(self):
        return self.surname


