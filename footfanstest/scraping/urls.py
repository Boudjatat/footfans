from django.urls import path
from scraping.views import PlayerListView,ClubListView, PlayerByClubView,DivisionListView,ClubByDivisionView


urlpatterns = [
    path("",DivisionListView.as_view(), name="division_list"),
    path("division_list/<division>/", DivisionListView.as_view(), name="division_list"),
    path("divisions/<int:pk>/clubs", ClubByDivisionView.as_view(), name="club_by_division"),
    path("club_list/<club>/",ClubListView.as_view(), name="club_list"),
    path("clubs/<int:pk>/players", PlayerByClubView.as_view(), name='player_by_club')

]

    
