from django.db.models.base import Model
from scraping.models import Player, Club, Division
from django.core.management.base import BaseCommand, CommandError
import requests
from bs4 import BeautifulSoup

def scrap_divsions(): # dummies !  It's not good...I know ! I will change the body of this function, I will scrape all informations from wikipédia 
    keys = ["name","country","link","pos"]
    divisions = list()
    
    values = ["Premier League","England","https://en.wikipedia.org/wiki/Premier_League",5]
    divisions.append(dict(zip(keys, values)))

    values = ["La liga","Spanish","https://en.wikipedia.org/wiki/La_Liga",4]
    divisions.append(dict(zip(keys, values)))

    values = ["Bundesliga","Germany","https://en.wikipedia.org/wiki/Bundesliga",1]
    divisions.append(dict(zip(keys, values)))

    values = ["Serie A","Italy","https://en.wikipedia.org/wiki/Serie_A",0]
    divisions.append(dict(zip(keys, values)))  

    values = ["Ligue 1","France","https://en.wikipedia.org/wiki/Ligue_1",1]
    divisions.append(dict(zip(keys, values)))
  
    return divisions


def scrap_clubs():
    divisions = scrap_divsions()
    
    keys = ["name","country","link","division"]
    clubs = list()

    for division in divisions:

        if not requests.get(division["link"]).ok:
            exit()
        
        page = BeautifulSoup(requests.get(division["link"]).text,"html.parser")
        
        tables = list()
        tables = page.find_all("table",class_="wikitable")

        tab = tables[division["pos"]]
        trs = tab.find_all("tr")

        lines = list()
        for tr in trs:
            a = tr.find("a")
            lines.append(a)

        del lines[0]

        links = [lines[i].get("href", "/") for i in range(len(lines)) if lines[i]]
        links = ["https://en.wikipedia.org" + str(link) for link in links]
            
        
        for i in range(len(links)):
            values = [lines[i].string,division["country"],links[i],division["name"]]
            clubs.append(dict(zip(keys,values)))

    return clubs


def Extract_player_Data(club): 

    soup = BeautifulSoup(requests.get(club["link"]).text,"html.parser")

    tables = soup.find_all("table", class_="wikitable football-squad nogrid")

    trs1 = tables[0].find_all("tr")
    del trs1[0]

    trs2 = tables[1].find_all("tr")
    del trs2[0]

    trs = trs1 + trs2
    
    Data_players = list()

    for tr in trs:
        Data_players.append(tr.find_all("td"))

    return Data_players

def scrap_players():
    keys = ["name","number","position","nationality","club","division"]
    players =list()

    clubs = scrap_clubs()
    
    for club in clubs:
        link_club = club["link"]
        players_Data = Extract_player_Data(club)

        for player in players_Data:
 
            values = [
                player[3].find("a").string if player[3].find("a") != None else None,
                player[0].string.strip('\n') if player[0].string.strip('\n') != '—' else 0,
                player[1].find("a").string,
                player[2].find("a").string,
                club["name"],
                club["division"]
            ]
            players.append(dict(zip(keys, values)))

    return players

class Command(BaseCommand):    
 
    def handle(self, *args, **options):
        #Player.objects.all().delete()
        #Club.objects.all().delete()    
        #Division.objects.all().delete()

        divisions = scrap_divsions()
        all_clubs = scrap_clubs()
        all_players = scrap_players()

        for player in all_players:
            for club in all_clubs:
                for division in divisions:
                    if player["club"] == club["name"] and  club["division"] == division["name"]:
                        #print(player)
                        try:
                            d = Division.objects.get(
                                name=division["name"],
                                country=division["country"],
                                link=division["link"]
                                )
                        except Division.DoesNotExist:
                            d = Division(
                                name=division["name"],
                                country=division["country"],
                                link=division["link"]
                                )
                            #d.save()
                            Division.objects.bulk_update(d)
                            self.stdout.write(self.style.SUCCESS("Divison has been added successfully "))

                        try:                 
                            c = Club.objects.get(
                                name=club["name"],
                                division=d,
                                link=club["link"]
                                )
                        except Club.DoesNotExist:
                            c = Club(
                                name=club["name"],
                                division=d,
                                link=club["link"]
                                )
                            #c.save()
                            Club.objects.bulk_update(c)
                            self.stdout.write(self.style.SUCCESS("Club has been added successfully "))

                        try:
                            p = Player.objects.get(
                                name=player["name"],
                                number=player["number"],
                                country=player["nationality"],
                                position=player["position"],
                                club=c
                                )
                        except Player.DoesNotExist:
                            p = Player(
                                name=player["name"],
                                number=player["number"],
                                country=player["nationality"],
                                position=player["position"],
                                club=c
                                )
                            #p.save()
                            Player.objects.bulk_update(p)
                            self.stdout.write(self.style.SUCCESS("Player has been added successfully "))

        print(Division.objects.all().count())
        print(Club.objects.all().count())
        print(Player.objects.all().count())    
