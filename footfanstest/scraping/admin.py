from django.contrib import admin
from .models import Division, Club, Player

# Register your models here.
admin.site.register(Division)
admin.site.register(Club)
admin.site.register(Player)
