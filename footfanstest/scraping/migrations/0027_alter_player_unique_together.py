# Generated by Django 3.2.4 on 2021-07-05 12:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scraping', '0026_auto_20210705_0854'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='player',
            unique_together={('number', 'position', 'name', 'surname', 'club')},
        ),
    ]
