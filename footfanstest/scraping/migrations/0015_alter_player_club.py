# Generated by Django 3.2.4 on 2021-06-30 11:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scraping', '0014_alter_player_club'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='club',
            field=models.CharField(max_length=50),
        ),
    ]
