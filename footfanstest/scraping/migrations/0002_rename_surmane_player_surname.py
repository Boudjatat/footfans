# Generated by Django 3.2.4 on 2021-06-22 23:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scraping', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='player',
            old_name='surmane',
            new_name='surname',
        ),
    ]
