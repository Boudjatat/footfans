from django.views.generic import ListView
from django.core.paginator import Paginator
from scraping.models import Player, Club, Division
from django.shortcuts import render

class  DivisionListView(ListView):
    model = Division

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class  ClubListView(ListView):

    model = Club
    paginate_by = 10
    
    def get_context_data(self, **kwargs):
        context = super(ClubListView, self).get_context_data(**kwargs)
        return context


class ClubByDivisionView(ListView):
    model = Club
    paginate_by = 10
    template_name = "scraping/club_list.html"

    def get_queryset(self, queryset=None):
        return super().get_queryset().filter(division=self.kwargs['pk'])
        

class  PlayerListView(ListView):

    model = Player
    paginate_by = 15

    def get_context_data(self, **kwargs):
        context = super(PlayerListView, self).get_context_data(**kwargs)
        return context

class PlayerByClubView(ListView):
    model = Player
    paginate_by = 10
    template_name = "scraping/player_list.html"

    def get_queryset(self, queryset=None):
        return super().get_queryset().filter(club=self.kwargs['pk'])
